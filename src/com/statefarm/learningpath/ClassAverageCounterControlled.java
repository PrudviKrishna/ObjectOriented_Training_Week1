package com.statefarm.learningpath;

//ClassAverageCounterControlled.java
//Solving the class average problem using the counter-controlled repetition.
import java.util.Scanner; // program uses Scanner class

public class ClassAverageCounterControlled {

	// main method begins the execution of java application
	public static void main(String[] args) {

		// creates a scanner object to obtain input from command window
		Scanner input = new Scanner(System.in);

		// Initialization phase
		int total = 0;        // initialize sum of grades entered by user
		int gradeCounter = 1; // Initialize # of grades to be entered next
		
		// processing phase uses counter-controlled repetition
		while (gradeCounter <= 10) {    // loop 10 times
			System.out.print("Enter the grade: "); // prompt
			int grade = input.nextInt();  // input next grade
			total += grade;  // add grade to total
			++gradeCounter; //increment counter by 1
		}

		// termination phase
		int average = total / 10; // integer division yields integer result
		
		// display total and average of grades
		System.out.printf("%n Total of 10 grades is %d%n", total);
		System.out.printf("Class Average is: %d%n", average);

	} // end of main method

} // end of class
