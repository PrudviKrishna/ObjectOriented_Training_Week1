package com.statefarm.learningpath;

public class AutoPolicyTest {

	public static void main(String[] args) {
		
		AutoPolicy policy1 = new AutoPolicy(111111111, "Toyota Camry", "NJ");
		AutoPolicy policy2 = new AutoPolicy(222222222, "Chevi Cruze", "ME");
		
		policyInNoFaultState(policy1);
		policyInNoFaultState(policy2);
		
}
		// method that displays whether an AutoPolicy is in a state with no-fault auto insurance
	public static void policyInNoFaultState(AutoPolicy policy)
	{
	System.out.printf("%s %s no-fault state%n", policy.getState(), (policy.isNoFaultState() ?"is":"is not"));
		
	
	
	}

}
