package com.statefarm.learningpath;

import java.util.Scanner; //program uses Scanner class

// AccountTest.java
public class AccountTest {

	public static void main(String[] args) {

		Account account1 = new Account("Prudvi Krishna", 150.0);
		Account account2 = new Account("John Snow", -7.0);

		// display initial balance of each object
		System.out.printf("%s balance is: %.2f%n", account1.getName(), account1.getBalance());
		System.out.printf("%s balance is: %.2f%n", account2.getName(), account2.getBalance());

		// create a scanner object to obtain input from command window
		Scanner input = new Scanner(System.in);

		System.out.print("Enter deposit amount for account1: "); // prompt
		double depositAmount = input.nextDouble(); // read user input
		System.out.printf("%nAdding deposit amount $%.2f to the account1 balance%n%n", depositAmount);
		account1.deposit(depositAmount); // add to account1 balance

		// display balances
		System.out.printf("%s balance is $%.2f%n", account1.getName(), account1.getBalance());
		System.out.printf("%s balance is $%.2f%n", account2.getName(), account2.getBalance());

		System.out.print("Enter deposit amount for account2: "); //prompt
		double depsoitAmount = input.nextDouble(); // read input from user
		System.out.printf("%nAdding deposit amount $%.2f to the account2 balance%n%n", depositAmount);
		account2.deposit(depositAmount); // add to account2 balance

		// display balances
		System.out.printf("%s balance is $%.2f%n", account1.getName(), account1.getBalance());
		System.out.printf("%s balance is $%.2f%n", account2.getName(), account2.getBalance());
	} // end of main method

} // end of class AccountTest
