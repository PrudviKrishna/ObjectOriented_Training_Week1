package com.statefarm.learningpath;

public class StudentTest {

	public static void main(String[] args) {
		Student account1 = new Student("Prudvi Krishna", 90.0);
		Student account2 = new Student("John Snow", 75.0);

		System.out.printf("%s's letter grade is: %s%n", account1.getName(), account1.getLetterGrade());
		System.out.printf("%s's letter grade is: %s%n", account2.getName(), account2.getLetterGrade());
	}

} // end class StudentTest
