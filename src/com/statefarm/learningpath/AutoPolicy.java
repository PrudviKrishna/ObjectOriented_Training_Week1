package com.statefarm.learningpath;

// AccountPolicy.java
public class AutoPolicy {

	// Declaration Phase
	private int accountNumber; // policy account number
	private String makeAndModel; // car that the policy applies to
	private String state; // two-letter state abbreviation
	
	// constructor
	public AutoPolicy(int accountNumber, String makeAndModel, String state) {
		super();
		this.accountNumber = accountNumber;
		this.makeAndModel = makeAndModel;
		this.state = state;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getMakeAndModel() {
		return makeAndModel;
	}

	public void setMakeAndModel(String makeAndModel) {
		this.makeAndModel = makeAndModel;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	// predicate method returns whether the state has no-fault insurance
	public boolean isNoFaultState()
	{
		boolean noFaultState = true;
		
		// determines whether state has no-fault  auto insurance
		switch (getState())
		{
		case "NH": case "VM": case "CT": case "RI": case "ME":
			noFaultState =  false;
			 break;
			 
		case "MA": case "NJ": case "NY": case "PA": 
			noFaultState = true;
			 break;
		}
		
	return noFaultState;	
	}

	
	

}
