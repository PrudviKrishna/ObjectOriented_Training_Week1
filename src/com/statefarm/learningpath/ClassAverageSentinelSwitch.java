package com.statefarm.learningpath;

import java.util.Scanner;

public class ClassAverageSentinelSwitch {

	public static void main(String[] args) {

		int grade;
		int total = 0;
		int gradeCounter = 0;
		int gradeA = 0;
		int gradeB = 0;
		int gradeC = 0;
		int gradeD = 0;
		int gradeE = 0;
		int gradeF = 0;

		Scanner input = new Scanner(System.in);

		System.out.printf("%s%n%s %s%n%s%n", "Enter the integer grades in the range 0-100.",
				"Type the end-of-file indicator to terminate the input:",
				"On UNIX/LINUX/MAC OS Xtype <Ctrl> d then press Enter", "On Windows press <Ctrl> z and press Enter");

		while (input.hasNext()) {

			grade = input.nextInt();
			total += grade;
			++gradeCounter;

			switch (grade / 10)

			{
			case 100: // grade was between 90 and 100 inclusive
			case 9:
				++gradeA;
				break; // exists switch

			case 8: // grade was between 80 and 89 inclusive
				++gradeB;
				break; // exists switch

			case 7: // grade was between 70 and 79 inclusive
				++gradeC;
				break; // exists switch
			case 6: // grade was between 60 and 69 inclusive
				++gradeD;
		        break; // exists switch
		        
			default: // grade was less than 60
				++gradeF;
				break; // exists switch
			} // end switch
		} // end while

		// Display Grade Report
		System.out.printf("%nGrade Report: %n%n");

		// Calculate average of all grades entered
		if (gradeCounter != 0) {
			double average = (double) total / gradeCounter;
			System.out.printf("Total of the %d grades entered is %d%n", gradeCounter, total);
			System.out.printf("Class average is %.2f%n", average);
			
			// output summary of results
			System.out.printf("%n%s%n%s%d%n%s%d%n%s%d%n%s%d%n%s%d%n", "Number of students who received each grade:",
					"A: ", gradeA,  // display number of A grades
					"B: ", gradeB,  // display number of B grades
					"C: ", gradeC,  // display number of C grades
					"D: ", gradeD,  // display number of D grades
					"F: ", gradeF); // display number of F grades
		} // end if
		else // no grades were entered, so output appropriate message
			System.out.println("No grades were entered");

	} // end of main method

} // end of class
