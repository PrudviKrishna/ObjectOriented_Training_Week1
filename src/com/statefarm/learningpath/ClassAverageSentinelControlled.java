package com.statefarm.learningpath;
//ClassAverageSentinelControlled.java
//Solving the class average problem using the Sentinel-Controlled repetition.
import java.util.Scanner; // program uses Scanner class

public class ClassAverageSentinelControlled {

	// main method begins the execution of java program
	public static void main(String[] args) {
		
		// creates scanner object to read input from command window
		Scanner input = new Scanner(System.in);
		
		// initialization phase
		int total = 0; // initialize sum of grades entered by user
		int counter = 0; // initialize the # of grades to be entered next

		System.out.print("Enter the first grade or -1 to quit: ");
		int grade = input.nextInt();
		
		// processing phase uses Sentinel-Controlled repetition
		while( grade != -1) // loops till grade != -1
		{
			total += grade; // add grade to the total
			++counter; // increment counter by 1
			System.out.print("Enter next grade or -1 to quit: ");// prompt
			grade = input.nextInt(); // input next grade
		}
		
		// termination phase
		if (counter != 0)
		{
			int average = total / counter; // integer division yields integer value
			
			// display average value
			System.out.printf("%nAverage of the grades is %d%n", average);
		} else System.out.println("No grades were printed");
	} // end of main method

} // end of class
