package com.statefarm.learningpath;

//Addition.Java
//Reading two integers from user and adding them and displaying total to console/command window.
import java.util.Scanner; //program uses class scanner

public class Addition {

	// main method begins the execution of Java Application
	public static void main(String[] args) {

		// create a Scanner object to obtain input from the command window.
		Scanner input = new Scanner(System.in);

		int number1; // first number/integer to add.
		int number2; // second number/integer to add.
		int sum; // sum of number1 and number2.

		System.out.print("Enter the first Integer: "); // prompt
		number1 = input.nextInt(); // read the first integer from user

		System.out.print("Enter second Integer: "); // prompt
		number2 = input.nextInt(); // read the second integer from user

		sum = number1 + number2; // add numbers and then store total in sum

		System.out.printf("Sum of two integers is %d%n", sum); // display sum

	} // end of method main

} // end of class Addition
