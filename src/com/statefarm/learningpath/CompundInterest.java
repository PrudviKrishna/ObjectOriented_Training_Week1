package com.statefarm.learningpath;

// CompundInterest.java
// Calculating Compound Interest using for-loop
public class CompundInterest {

	// main method begins execution of java program
	public static void main(String[] args) {

		// Initialization phase
		double amount; // amount on deposit at end of each year
		double interestRate = 0.05; // interest rate
		double principal = 1000.0; // initial amount before interest

		// display headers
		//System.out.printf("%s%20s%n", "year", "Amount on deposit");
		
		// calculating amount on deposit at end of each year of 10 years
		for (int year = 1; year <= 10; year++) {
			// calculate new amount for specified year
			amount = principal * Math.pow(1 + interestRate, year);
			System.out.printf("The money in the account at year %d is: %,.2f%n", year, amount);
		}

	}

}
