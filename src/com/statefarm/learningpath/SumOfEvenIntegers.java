package com.statefarm.learningpath;

//SumOFEvenIntegers.java
// Adding the even integers in the range 2 to 20 using for statement
public class SumOfEvenIntegers {

	// main method begins the execution of java program
	public static void main(String[] args) {

		// initialization of sum
		int sum = 0;
		// processing phase
		// counter variable is i which is initialized with 2 and
		// loops until loop condition fails and each time control variable increments
		// by 2
		for (int i = 2; i <= 20; i += 2) {
			sum += i;
		}
		// Display sum of even numbers from 20 to 20
		System.out.printf("Sum of even numbers from 2 to 20 is : %d%n", sum);

	} // end of main method

} // end of class
