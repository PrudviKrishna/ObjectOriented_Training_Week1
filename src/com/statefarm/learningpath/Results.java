package com.statefarm.learningpath;

import java.util.Scanner;

// Results.java
// This program calculates the test results and 
public class Results {

	// main method starts the execution of java application
	public static void main(String[] args) {

		// creates scanner object to read the input from command window
		Scanner input = new Scanner(System.in);

		// initialization Phase
		int passes = 0; // initializing passes to zero
		int failures = 0; // initializing failures to zero
		int studentCounter = 1; // initializing student counter to zero

		// processing phase
		// process 10 students using counter-controlled loop
		while (studentCounter <= 10) {
			System.out.print("Enter the exam result (1 = pass and 2 = fail): "); // prompt the user to enter next exam result
			int examResult = input.nextInt(); // storing the exam result in examResult

			if (examResult == 1) {
				passes += 1; // increment the passes 
			} else
				failures += 1;

			++studentCounter; // increment the student counter
		}
		
		// Display the number of passes and failures
		System.out.printf("%nTotal number of passes is: %d%n", passes);
		System.out.printf("%nTotal number of failures is: %d%n%n", failures);
		
		// termination phase
		if ( passes >= 8)
		{
			System.out.println("Bonus to instructor!");
		}
	}

	 
}
