package com.statefarm.chapter5;

// program uses scanner class
import java.util.Scanner;

public class Sales {

	// main method starts the execution of java program
	public static void main(String[] args) {
		
		double product1 = 2.98;
		double product2 = 4.50;
		double product3 = 9.98;
		double product4 = 4.49;
		double product5 = 6.87;
		
		double product1Total = 0.0;
		double product2Total = 0.0;
		double product3Total = 0.0;
		double product4Total = 0.0;
		double product5Total = 0.0;
		
		// creates scanner object to read input from the command window
		Scanner input = new Scanner(System.in);
		
		// prompt the user to enter product number and quantity of products sold
		System.out.println("Enter the product number and quantity sold: "); 
		
	
		
		while (input.hasNext())
		{
		
	    String productSold = input.nextLine(); //read the values from command window
		
		// splitting the products and quantity sold using split
		String[] productSoldSplitter = productSold.split("\\s+"); 
		
		// storing the product number in productNum
		int productNum = Integer.parseInt(productSoldSplitter[0]);
		
		// storing the quantity sold in quantitySold
		int quantitySold = Integer.parseInt(productSoldSplitter[1]);   
		
switch (productNum)
{
case 1:
	product1Total += product1 * quantitySold;
	//System.out.printf("The retail price of product 1 is %,.2f%n", product1 * quantitySold);
	break;
case 2:
	product2Total += product2 * quantitySold;
	//System.out.printf("The retail price of product 2 is %,.2f%n", product2 * quantitySold);
	break;
case 3:
	product3Total += product3 * quantitySold;
	//System.out.printf("The retail price of product 3 is %,.2f%n", product3 * quantitySold);
	break;
case 4:
	product4Total += product4 * quantitySold;
	//System.out.printf("The retail price of product 4 is %,.2f%n", product4 * quantitySold);
	break;
case 5:
	product5Total += product5 * quantitySold;
	//System.out.printf("The retail price of product 5 is %,.2f%n", product5 * quantitySold);
	break;
default:
	System.out.println("No products are sold!");
	break;
}

		}
		
		System.out.printf("The total retail price of product 1 is %,20.2f%n", product1Total);
		System.out.printf("The total retail price of product 2 is %,20.2f%n", product2Total);
		System.out.printf("The total retail price of product 3 is %,20.2f%n", product3Total);
		System.out.printf("The total retail price of product 4 is %,20.2f%n", product4Total);
		System.out.printf("The total retail price of product 5 is %,20.2f%n", product5Total);
		
		double totalSold = product1Total + product2Total + product3Total + product4Total + product5Total;
		
	System.out.printf("%nThe total retail price of all products sold is: %,20.2f%n", totalSold);
	}
	

}
