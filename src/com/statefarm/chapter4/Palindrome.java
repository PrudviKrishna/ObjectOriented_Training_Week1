package com.statefarm.chapter4;

import java.util.Scanner;

//Program to check whether the given 5 digit number is palindrome or not
//Palindrome reads the same forward and backward.
public class Palindrome {

	public static void main(String[] args) {
		/*
		 * Create a Scanner object that reads data from keyboard declare a variable to
		 * store the input value check the input value whether it is 5 digits or not
		 * true == 5 digits Check whether the input 5 digit value is palindrome or not
		 * 1. reverse the digit 2. Compare the original input with reverse value 3.
		 * Display input is palindrome or not. false Prompt the user to display 5 digit
		 * value.
		 */

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter the 5 digit number: ");

		int number = scan.nextInt();
		// String string = String.valueOf(number);

		while (String.valueOf(number).length() != 5) {
			System.out.println("Please enter 5 digit number: ");
			number = scan.nextInt();
		}

		int n = number;
		int reverse = 0;
		int r;
		int q;

		while (n > 0) {
			r = n % 10;
			q = n / 10;
			reverse = (reverse * 10) + r;
			n = q;

		}

		if (number == reverse)
			System.out.println("Given 5 digit number is Palindrome");
		else
			System.out.println("Given 5 digit number is not Palindrome");

	}
}
