package com.statefarm.chapter2;

import java.util.Scanner;

public class ArithmeticAndAverage {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		int number1;
		int number2;
		int number3;
		int average;
		int sum;
		int product;

		System.out.print("Enter first integer: ");
		number1 = scan.nextInt();

		System.out.print("Enter second integer: ");
		number2 = scan.nextInt();
		
		System.out.print("Enter third  integer: ");
		number3 = scan.nextInt();

		sum = number1 + number2 + number3;
		product = number1 * number2 * number3;
		average = (number1 + number2 + number3)/3;
		
		if((number1 > number2) && (number1 > number3))
			System.out.println("number1 is largest");
			
		else if((number2 > number1) && (number2 > number3))
			System.out.println("number2 is largest");
			
		
		else //if((number3 > number1) && (number3 > number2))
			System.out.println("number3 is largest");
		
		if((number1 < number2) && (number1 < number3))
			System.out.println("number1 is smallest");
			
		else if((number2 < number1) && (number2 < number3))
			System.out.println("number2 is smallest");
			
		
		else //if((number3 < number1) && (number3 < number2))
			System.out.println("number3 is smallest");
			

		System.out.println("Addition of three numbers is: " + sum);
	
		System.out.println("Product of three numbers is: " + product);
		
		System.out.println("Average of three numbers is: " + average);
	

	}

}
