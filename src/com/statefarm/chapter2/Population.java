package com.statefarm.chapter2;

import java.util.Scanner;

public class Population {

	public static void main(String[] args) {
		
		double currentPopulation;
		double growthRate;
		int years;
		double estimatedPopulation;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the current world population(in billions): ");
		currentPopulation = scan.nextDouble();

		System.out.print("Enter the annual growth rate of the world population: ");
		growthRate = scan.nextDouble()/100;
 
		System.out.print("Enter the number of years to calculate world population: ");
		years = scan.nextInt();
		 
		estimatedPopulation = currentPopulation;
		for(int i = 0; i <years; i++) {
			
		estimatedPopulation += estimatedPopulation * growthRate;
		}
		System.out.println("Estimated world population: " + estimatedPopulation);
	}

}
