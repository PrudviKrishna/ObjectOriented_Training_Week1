package com.statefarm.chapter2;

import java.util.Scanner;

public class Circle {

	public static void main(String[] args) {

		int radius;
		// double diameter;
		// double circumference;
		// double area;

		Scanner scan = new Scanner(System.in);

		// diameter = 2 * radius;
		// area = Math.PI * radius * radius;
		// circumference = 2 * Math.PI * radius;

		System.out.print("Enter the radius of a circle: ");
		radius = scan.nextInt();
		System.out.printf("The diameter of a circle is: %d%n", 2 * radius);
		System.out.printf("The circumference of a circle is: %f%n", 2 * Math.PI * radius);
		System.out.printf("The area of a circle is: %f%n", Math.PI * radius * radius);

	}

}
