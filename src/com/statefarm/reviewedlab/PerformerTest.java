package com.statefarm.reviewedlab;

public class PerformerTest {

	public static void main(String[] args) {
				
		Performer[] performer = new Performer[7];
		for (int i = 0; i < 4; i++)
		{
			performer[i] = new Performer(100 + i);
		}
		
		performer[4] = new Dancer(200, "Tap");
		performer[5] = new Dancer(201, "Break");
		
		performer[6] = new Vocalist(300, "G");
	
		for (Performer contestant : performer)
		{
			System.out.println(contestant.perform());
		}
		
		
		
//		Performer contestant1 = new Performer(100);
//		Performer contestant2 = new Performer(101);
//		Performer contestant3 = new Performer(102);
//		Performer contestant4= new Performer(103);
//		
//		Dancer dancer1 = new Dancer(200, "Tap");
//		Dancer dancer2 = new Dancer(201, "Break");
//		
//		Vocalist vocalist = new Vocalist(300, "G");
//
//		System.out.println(contestant1.perform());
//		System.out.println(contestant2.perform());
//		System.out.println(contestant3.perform());
//		System.out.println(contestant4.perform());
//		
//		System.out.println(dancer1.perform());
//		System.out.println(dancer2.perform());
//		
//		
//		System.out.println(vocalist.perform());
//		System.out.println(vocalist.givenVolume(5));
		
		

	}

}
