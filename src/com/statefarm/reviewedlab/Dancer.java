package com.statefarm.reviewedlab;

public class Dancer extends Performer {

	//private int id;
	private String style;
	
	public Dancer(int id, String style) {
		super(id);
		//this.id = id;
		this.style = style;
	}
	
	
//	public int getId() {
//		return id;
//	}


//	public void setId(int id) {
//		this.id = id;
//	}


	public String getStyle() {
		return style;
	}


	public void setStyle(String style) {
		this.style = style;
	}


	@Override
	public String perform()
	{
		return style + " - " + id + " - dancer";
	}
	
}
