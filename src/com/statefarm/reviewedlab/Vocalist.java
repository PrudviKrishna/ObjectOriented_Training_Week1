package com.statefarm.reviewedlab;

public class Vocalist extends Performer {
	
	// private int id;
	private String key;
	
	public Vocalist (int id, String key) {
		super(id);
		//this.id = id;
		this.key = key;
	}
	
//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public String perform()
	{
		return "I sing in the key of"+ " - " + key + " - " + id;
	}
	
	public String givenVolume(int volume)
	{
		return "I sing in the key of "+ " - " + key + " - " + "at the volume " + volume + " - " + id;
	}
	
	
}
