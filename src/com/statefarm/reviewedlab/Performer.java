package com.statefarm.reviewedlab;

// Performer.java
// Performer class contains details about the performer 
public class Performer {

	protected int id;
	
	public Performer(int id)
	{
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String perform()
	{
		return id + "-performer";
	}
	
	
}
