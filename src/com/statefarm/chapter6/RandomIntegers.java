package com.statefarm.chapter6;

import java.security.SecureRandom; // program uses classSecureRandom

// Shifted and scaled random integers
public class RandomIntegers {

	public static void main(String[] args) {

		// randomNUmbers object will produce secure random numbers
		SecureRandom randomNumbers = new SecureRandom();

		// loop 20 times
		for (int counter = 1; counter <= 20; counter++) {

			// pick random integer from 1 to 6
			int face = 1 + randomNumbers.nextInt(6);

			System.out.printf("%d ", face); // display generated value
			if (counter % 5 == 0) {
				System.out.println();
			}

		}

	}

}
