package com.statefarm.chapter7;

import java.security.SecureRandom;

public class DeckOfCards {

	private Card[] deck;
	private int currentCard;
	private static final int NUMBER_OF_CARDS = 52;
	private static final SecureRandom randomNumbers = new SecureRandom();
	
	// constructor fills deck of cards
	public DeckOfCards ()
	{
		
		String[] faces = { "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		
		deck = new Card[NUMBER_OF_CARDS]; // create array of card Objects
		currentCard = 0; // first card dealt will be deck[0];
		
		// populate the deck with Card objects
		for (int count = 0; count < deck.length; count++)
		{
			deck[count] = new Card(faces[count % 13], suits[count / 13]);
		}
	}
	
	// shuffle deck of cards with one-pass algorithm
	public void Shuffle()
	{ 
		currentCard = 0;
		// for each card, pick another random card (0-51) and swap them.
		for (int first = 0; first < deck.length; first++)
		{
			// select random number between 0 and 51
			int second = randomNumbers.nextInt(NUMBER_OF_CARDS);
			
			// swap current card with randomly selected card
			Card temp = deck[first];
			deck[first] = deck[second];
			deck[second] = temp;
		}
	}
	
	// deal one card 
	public Card dealCard()
	{

		// determine whether cards left to be dealt
		// compare current card with length of deck array.
		if (currentCard < deck.length)
		{
			return deck[currentCard]; // return current card in array
		}
		else return null; // return null to indicate that all cards are dealt
		
	}
	
	
	
} // end class DeckOfCards
