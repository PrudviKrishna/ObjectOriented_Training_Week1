package com.statefarm.chapter7;

// GradeBook class using a 2-D array to store grades.
public class GradeBook2D {

	private String courseName; // name of course this grade book represents
	private int[][] grades; // two-dimen array of student grades

	public GradeBook2D(String courseName, int[][] grades) {
		this.courseName = courseName;
		this.grades = grades;

	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public void processGrades() {
		// output grades array
		outputGrades();


		// call methods getMinimum and getMaximum
		System.out.printf("%nLowest grade is %d%nHighest grade is %d%n%n", getMinimum(), getMaximum());

		// call outputBarChart to print grade distribution chart of all grades on all tests
		outputBarChart();
	}
	
	// find smallest grade
	public int getMinimum()
	{
		// assume first element of grades array is smallest
		int minimum = grades[0][0];
		
		for (int[] studentGrade : grades)
		{
			for (int grade : studentGrade)
			{
				if (grade < minimum)
				{
					minimum = grade;
				}
			}
		}
		return minimum;
}
	// find highest grade
	public int getMaximum()
	{
		// assume first element of grades array is highest
		int maximum = grades[0][0];
		
		for (int[] studentGrade : grades)
		{
			for (int grade : studentGrade)
			{
			 if (grade > maximum)
			 {
				 maximum = grade;
			 }
			}
		}
		
		return maximum;
	}
	// get average of particular set of grades
	public double getAverage(int[] setOfGrades)
	{
	     int total = 0;
	     
	     for (int grade : setOfGrades)
	     {
	    	 total += grade;
	     }
	     
	     return (double) total / setOfGrades.length;
	}

	// output bar chart displaying grade distribution
	public void outputBarChart()
	{
		System.out.println("Grade distribution:");
		
		// stores frequency of grades in each range of 10 grades
		int[] frequency = new int[11];
		
		// for each grade in GradeBook, increment the appropriate frequency
		
		for (int[] studentGrade : grades)
		{
		for (int grade : studentGrade	)
		{
			++frequency[grade / 10];
		}
		}
		
		// for each grade frequency, print bar in chart
		for (int count = 0; count < frequency.length; count++)
		{
			// output bar label ("00-09: ", ..., "90-99: ", "100: ")
			if (count == 10)
			System.out.printf("%5d: ", 100);
			else
				System.out.printf("%02d-%02d: ", count * 10, (count * 10) + 9);
			
			for (int star = 0; star < frequency[count]; star++)
			{
				System.out.print("*");	
			}
			
			System.out.println();
			
		}
	}
		
		
		public void outputGrades()
		{
			System.out.printf("The grades are: %n%n");
			System.out.print("        	"); // align column heads
			
			for (int test = 0; test < grades[0].length; test++)
			{
				System.out.printf("Test %d ", test + 1);
				
			}	
			
			System.out.println("Average"); // student average column heading
				for (int student = 0; student < grades.length; student++)
				{
				
				System.out.printf("%nStudent %2d", student + 1);
				
//				for (int grade = 0; grade < grades[0].length; grade++)
//				{
//					System.out.printf("%02d", grades[0][grade] );
//				}
				
				for (int grade : grades[student])
				{
					System.out.printf("%8d", grade);
				}
				
				double average = getAverage(grades[student]);
				
				System.out.printf("%9.2f%n", average);
				
				}
				
			}
	
} // end class GradeBook2D
