package com.statefarm.chapter7;

public class DeckOfCardsTest {

	public static void main(String[] args) {
		
		DeckOfCards myDeckOfCards = new DeckOfCards();
		myDeckOfCards.Shuffle(); // place cards in random manner

		// print all 52 cards in the order in which they are dealt
		for (int i = 1; i <= 52; i++ )
		{
			// display a card
			System.out.printf("%-20s", myDeckOfCards.dealCard() );
			
			if (i % 4 == 0)
			{
				System.out.println(); // output a new line for every fourth card 
			}
		}

	}

} // end of class DeckOfCardsTest
