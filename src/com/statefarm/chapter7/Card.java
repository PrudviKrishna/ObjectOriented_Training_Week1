package com.statefarm.chapter7;

// Card.java
public class Card {

	private String face;
	private String suit;
	
	// two-argument constructor to initialize face and suit
	public Card(String face, String suit)
	{
		this.face = face; // initializing face of card
		this.suit = suit; // initializing suit of card
	}
	
	// return string representation of card
	@Override
	public String toString() {
		return face + "of" + suit ; 
	}
	
} // end of class
