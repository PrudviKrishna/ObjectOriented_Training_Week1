package com.statefarm.chapter7;

public class GradeBookTest {

	public static void main(String[] args) {
		
		int[] gradesArray = {67, 78, 89, 98, 96, 92, 91, 87, 80, 74};
		GradeBook gradeBook = new GradeBook("CS101 Introduction to Java Programming", gradesArray);
		
		System.out.printf("Welcome to the grade book for:%n%s%n%n", gradeBook.getCourseName());
		gradeBook.processGrades();

	}

}
