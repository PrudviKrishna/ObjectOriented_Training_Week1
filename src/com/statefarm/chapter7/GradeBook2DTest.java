package com.statefarm.chapter7;

// GradeBook2DTest creates GradeBook2D object using a 2-D array of grades, then invokes method processGrades to analyze them.
public class GradeBook2DTest {

	public static void main(String[] args) {
		
		// 2-D array of student grades
		int[][] grades = {{87, 96, 70},
				{68, 87, 90},
				{94, 100, 90},
				{100, 81, 82},
				{83, 65, 85},
				{78, 87, 65},
				{85, 75, 83},
				{91, 94, 100},
				{76, 72, 84},
				{87, 93, 73}};
		
		GradeBook2D myGradeBook = new GradeBook2D("CS101 Introduction to Java Programming", grades);
		System.out.printf("Welcome to the grade book for%n%s%n%n", myGradeBook.getCourseName() );
		myGradeBook.processGrades();

	}

}
