package com.statefarm.chapter7;

import java.util.Arrays;

public class ArraysExcercise7_8 {

	public static void main(String[] args) {
        
		// ii. Initialize each of the five elements of one-dimensional integer array g to 8
		int[] g = new int[5];
		Arrays.fill(g, 8);
		
		for (int element : g)
		{
			System.out.printf("%d ", element );
		}
		
		System.out.println();
		
		
//		for (int index = 0; index < g.length; index++ )
//		{
//			if(index % 5 == 0)
//			{
//				g[index] = 8;
//			}
//		}
		
		
		
		
		// i. Display the value of element 6 in array f
		System.out.printf("%d ",  g[5] );
		
		// c. Total the 100 elements of floating-point array c
		double[] c = new double[100];
		
		double total = 0.0;
		for (double element : c)
		{
			total += element;
		}
		
		// d. copy 11-element array a into the first portion of array b which contains 34 elements.
		int[] a = new int[11];
		int[] b = new int[34];
		
		System.arraycopy(a, 0, b, 0, a.length);
		
		// e. Determine and display the smallest and largest values contains in 99-element floating-point array w
		
		double[] w = new double[99];
		
		double largest = w[0];
		double smallest = w[0];
		
		for (double element : w)
		{
			if (element > largest)
			{
				largest = element;
			} else if (element < smallest)
			{
				smallest = element;
			}
		}
		
		System.out.printf("%nThe smallest element in the array is: %.2f%nThe largest element in the array is: %.2f%n", smallest, largest);

	}

}
